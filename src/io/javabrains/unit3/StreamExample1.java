package io.javabrains.unit3;

import io.javabrains.unit1.Person;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class StreamExample1 {

    public static void main(String[] args) {
        List<Person> people = Arrays.asList(
                new Person("Charles","Dickens", 60),
                new Person("Lewis","Carroll", 42),
                new Person("Thomas","Carlyle", 51),
                new Person("Charlotte","Bronte", 60),
                new Person("Matthew","Arnold", 60)
        );
        //Allows multiple functions to performed at once on an object during an iteration
        //This makes One big loop with Multiple operations simple
        people.stream()
                .filter(p -> p.getSecondName().startsWith("C"))// if this is true proceed to the next part
                .forEach(p ->System.out.println(p.getFirstName()));

        /*
        A stream comprises of three things
        a source
        a filter operations
        a terminal operation , the end condition. This is what causes the streams to act
         */

        Stream<Person> p1 = people.stream();

       long count= people.parallelStream()// allows processing on multiple. It could split the collection to multiple cores and perform the same function
                .filter(p -> p.getSecondName().startsWith("D"))
                .count();
        //this is parallel processing
        System.out.println(count);
    }
}
