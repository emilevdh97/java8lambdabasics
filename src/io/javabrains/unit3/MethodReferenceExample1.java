package io.javabrains.unit3;

public class MethodReferenceExample1 {

    public static void main(String[] args) {
        printMessage();
        //example of method referencing
        Thread t  = new Thread(MethodReferenceExample1::printMessage);// it's the same as () -> printMessage();
        //Okay this is the implementation of the abstract function of runnable which is actually the function printmessage inside
        //MethodReferenceExample1
        t.start();
    }

    public static void printMessage(){
        System.out.println("Hello");
    }

}
