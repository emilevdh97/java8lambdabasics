package io.javabrains.unit3;

import io.javabrains.unit1.Person;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class MethodReferenceExample2 {

    public static void printMessage(Person p ){
        System.out.println(p);
    }

    public static void main(String[] args) {
        List<Person> people = Arrays.asList(
                new Person("Charles","Dickens", 60),
                new Person("Lewis","Carroll", 42),
                new Person("Thomas","Carlyle", 51),
                new Person("Charlotte","Bronte", 60),
                new Person("Matthew","Arnold", 60)
        );
        //Step 2: Create a method that prints all elements in the list
        System.out.println("Printing All persons");
        performConditional(people, p -> true, System.out::println);

    }

    private static void performConditional(List<Person> people, Predicate<Person> predicate, Consumer<Person> consumer) {
        for(Person p : people){
            if(predicate.test(p))
                consumer.accept(p);
        }
    }



}