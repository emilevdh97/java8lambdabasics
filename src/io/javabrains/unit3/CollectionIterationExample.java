package io.javabrains.unit3;

import io.javabrains.unit1.Person;

import java.util.Arrays;
import java.util.List;

public class CollectionIterationExample {

    public static void main(String[] args) {
        List<Person> people = Arrays.asList(
                new Person("Charles","Dickens", 60),
                new Person("Lewis","Carroll", 42),
                new Person("Thomas","Carlyle", 51),
                new Person("Charlotte","Bronte", 60),
                new Person("Matthew","Arnold", 60)
        );
        System.out.println("using for loop");
        for (int i = 0 ;i < people.size(); i++){
            System.out.println(people.get(i));
        }
        System.out.println("USing for each loop");
        for (Person p : people){
            System.out.println(p);
        }
        //these are called external iterators because we are managing the iterators

        //internal iterators
        //The run-time runs the iteration
        System.out.println("Using internal iterators");
        //Method references are used in place of Lambdas when the function is only thing you wish to do on the object
        //makes it's easier to run in multiple threads and parrellelism, which allows multiple functions to be executed at the same time
        people.forEach(System.out::println);
    }


   // public void print(Person p){
    //    System.out.println(p);
    //}


}
