package io.javabrains.unit1;

import java.util.*;

public class Unit1ExerciseJava7Answers {

    public static void main(String[] args) {
        List<Person> people = Arrays.asList(
                new Person("Charles","Dickens", 60),
                new Person("Lewis","Carroll", 42),
                new Person("Thomas","Carlyle", 51),
                new Person("Charlotte","Bronte", 60),
                new Person("Matthew","Arnold", 60)
        );
        //Step 1: Sort List by last name
        Collections.sort(people, new Comparator<Person>(){

            @Override
            public int compare(Person person, Person person2) {
                return person.getSecondName().compareTo(person2.getSecondName());
            }
        });
        //Step 2: Create a method that prints all elements in the list
        System.out.println("Printing All persons");
        printAll(people);
        //Step 3: Create a method that prints all people that have last name beginning with C
        System.out.println("Printing All persons with last name beginning with C");
        printConditional(people, new Conditional() {
            @Override
            public boolean test(Person p) {
                return p.getSecondName().startsWith("C");
            }
        });


        printConditional(people, new Conditional() {
            @Override
            public boolean test(Person p) {
                return p.getFirstName().startsWith("C");
            }
        });
    }

    private static void printConditional(List<Person> people, Conditional condition) {
        for(Person p : people){
            if(condition.test(p))
                System.out.println(p);
        }
    }

    private static void printAll(List<Person> people) {
        for(Person p : people){
            System.out.println(p);
        }
    }

}



interface Conditional{
    boolean test(Person p );
}