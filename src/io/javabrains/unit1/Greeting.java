package io.javabrains.unit1;

//The @FunctionalInterface annotation is completely optional. The Java compiler does not require it for your lambda types.
//But it is good practice to add it
@FunctionalInterface
public interface Greeting {
    public void perform();
}
