package io.javabrains.unit1;

import java.util.Collections;
import java.util.List;

public class SorterImpl implements Sorter {
    @Override
    public void sortByLastname(List<Person> people) {
     Collections.sort(people);
    }

    @Override
    public void printAllElements(List<Person> people) {
            for ( Person person : people){
                System.out.println(person.getFirstName());
                System.out.println(person.getSecondName());
                System.out.println(person.getAge());
             }
    }

    @Override
    public void printLastNameBeginningWithC(List<Person> people) {
        for ( Person person : people){
            if(person.getSecondName().startsWith("C")) {
                System.out.println(person.getFirstName());
                System.out.println(person.getSecondName());
                System.out.println(person.getAge());
            }
        }
    }
}
