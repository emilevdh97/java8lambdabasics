package io.javabrains.unit1;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Unit1Exercise {

    public void step1(List<Person> people, SorterImpl sorter){
        sorter.sortByLastname(people);
    }

    public void step2(List<Person> people, SorterImpl sorter){
        sorter.printAllElements(people);
    }

    public void step3(List<Person> people, SorterImpl sorter){
        sorter.printLastNameBeginningWithC(people);
    }

    public void java8(Lambda lambda,List<Person> people){
        lambda.execute(people);
    }

    public static void main(String[] args) {
    List<Person> people = Arrays.asList(
            new Person("Charles","Dickens", 60),
            new Person("Lewis","Carroll", 42),
            new Person("Thomas","Carlyle", 51),
            new Person("Charlotte","Bronte", 60),
            new Person("Matthew","Arnold", 60)
    );
        //Step 1: Sort List by last name

        //Step 2: Create a method that prints all elements in the list

        //Step 3: Create a method that prints all people that have last name beginning with C

        /**
         * Have the methods accept behavior instead of hard-coding them. For example, pass in behaviour to find if a person's last name begins with 'C'
         *
         * You can do it with Java 7 and Java 8 ways
         *
         */
        Unit1Exercise unit1Exercise = new Unit1Exercise();
        SorterImpl sorter = new SorterImpl();
        // Java 7
        unit1Exercise.step1(people,sorter);
        unit1Exercise.step2(people,sorter);
        unit1Exercise.step3(people,sorter);
        // Java 8
        Lambda lambda = p -> {
            Collections.sort(p);
        };
        unit1Exercise.java8(lambda,people);
        lambda = p -> {
            for ( Person person : p){
                System.out.println(person.getFirstName());
                System.out.println(person.getSecondName());
                System.out.println(person.getAge());
            }
        } ;
        unit1Exercise.java8(lambda,people);
        lambda = p -> {
            for ( Person person : p){
                if(person.getSecondName().startsWith("C")) {
                    System.out.println(person.getFirstName());
                    System.out.println(person.getSecondName());
                    System.out.println(person.getAge());
                }
            }
        } ;
        unit1Exercise.java8(lambda,people);
        //
    }
}
