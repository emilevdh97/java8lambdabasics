/**
 *Lambdas allow you to implement a function, functions can now be values
 *Lambdas introduce a new paradigm of programming to Java, this called functional programming
 *This promotes code that easy to read and implement , making programming easier and therefore better
 *Functions can be defined inline to a variable, this is lambdas( a block of code gets assigned to a variable
 */
package io.javabrains.unit1;

import static java.lang.System.*;

public class Greeter {

    public void greet(Greeting greeting){
        greeting.perform();
    }

    public static void main(String[] args) {
        //Greeter greeter = new Greeter();
        //HelloWorldGreeting helloWorldGreeting = new HelloWorldGreeting();
        //We are passing an object that has a behaviour, an action to the greet method and not necessarily a object. This was done in Java 7 without lambda's
        //Polymorphism(ability of an object to take on many forms) in action here
       // greeter.greet(helloWorldGreeting);
        //Lambdas allow you to create an action to pass into a function to be executed.

        //The Java compiler looks at the written lambda and determines the arguments and return types and matches it with
        //the a similiar function in the interface
        //MyLambda myLambdaFunction =() -> System.out.println("hello World");
        //Greeting myLambdaFunction =() -> System.out.println("hello World");

       // myAdd addFunction = (int a , int b) -> a+b;


        //Episode 9, interface implementations
       // Greeter greeter = new Greeter();
        //instance of the specific implementation
        //Greeting helloWorldGreeting = new HelloWorldGreeting();
        // Lambda expression, we are skipping the class by implementing the method directly
//        Greeting lambdaGreeting =() -> System.out.println("hello World");
        //This is how you execute Lambda Expressions.By calling the interface method on it,
        // just as if it were an instance of a class!


        //Instead of creating a class we could just create a anonymous inner class, inline implementation of an interface
        //Very similiar to Lambdas but not the same
       // Greeting innerClassGreeting = new Greeting() {
        //    @Override
//            public void perform() {
//                System.out.println("Hello World!");
//            }
//        };
//        //Lambda is it's on thing. It is not a implementation of an interface and it's not a shortcut for anonymous classes
//        greeter.greet(lambdaGreeting);
//        greeter.greet(innerClassGreeting);

        //Episode 10, type interface
        Greeter greeter = new Greeter();
        Greeting lambdaGreeting =() -> System.out.println("hello World");
        Greeting innerClassGreeting = new Greeting() {
            @Override
            public void perform() {
                System.out.println("Hello World!");
            }
        };
        greeter.greet(lambdaGreeting);
        greeter.greet(innerClassGreeting);

    }
}

// Only one lambda implementation per functional interface
//Only the name of the interface matters and not the function name. the interface name is used when declaring
//interface MyLambda{
    //This will be know as a functional interface function. It's used for functional programming
  //  void foo();
//}

//interface myAdd{
//    int add(int x ,int y);
//}