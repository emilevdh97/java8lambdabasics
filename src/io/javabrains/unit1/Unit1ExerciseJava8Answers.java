package io.javabrains.unit1;

import java.util.*;

public class Unit1ExerciseJava8Answers {

    public static void main(String[] args) {
        List<Person> people = Arrays.asList(
                new Person("Charles","Dickens", 60),
                new Person("Lewis","Carroll", 42),
                new Person("Thomas","Carlyle", 51),
                new Person("Charlotte","Bronte", 60),
                new Person("Matthew","Arnold", 60)
        );
        //Step 1: Sort List by last name
        //The complier knows what type should be there
        Collections.sort(people,( o1, o2)->o1.getSecondName().compareTo(o2.getSecondName()));
        //Step 2: Create a method that prints all elements in the list
        System.out.println("Printing All persons");
        printConditional(people, p -> true);
        //Step 3: Create a method that prints all people that have last name beginning with C
        System.out.println("Printing All persons with last name beginning with C");
        printConditional(people, p -> p.getSecondName().startsWith("C"));


        printConditional(people, p -> p.getFirstName().startsWith("C"));
    }

    private static void printConditional(List<Person> people, Conditional condition) {
        for(Person p : people){
            if(condition.test(p))
                System.out.println(p);
        }
    }



}