package io.javabrains.unit1;

public class TypeInferenceExample {

    public static void main(String[] args) {
        //you dont have to specify the argument type and if you only have one argument, you do not need to specify the parenthesis
        //compilier looks to the function signature in the functional interface for the parameters and return
           // StringLengthLambda myLambda= s -> s.length();
        printLambda(s -> s.length());
    }

    public static void printLambda(StringLengthLambda l){
        System.out.println(l.getLength("HelloLambda"));
    }
    interface StringLengthLambda{
        int getLength(String s);
    }
}
