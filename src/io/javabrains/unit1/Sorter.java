package io.javabrains.unit1;

import java.util.List;

public interface Sorter {

    public void sortByLastname(List<Person> people);
    public void printAllElements(List<Person> people);
    public void printLastNameBeginningWithC(List<Person> people);
}
