package io.javabrains.unit1;

public class RunnableExample {
    public static void main(String[] args) {
        //Episode 11 will show how to use Lambdas to create a new thread and pass to it a instance of runnable
        Thread myThread = new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Printed inside runnable");
            }
        });
    //instead of doing the above we can create a lambda expression
        //Remember, this works because Runnable has a single method. If it had more than one method, you could not have written a lambda function of that type
        Runnable lambdaRunnable = () -> System.out.println("Printed inside Lambda Expression");
        Thread myLambdaThread = new Thread(() -> System.out.println("Printed inside Lambda Expression"));
        myLambdaThread.run();

        myThread.run();
    }
}
