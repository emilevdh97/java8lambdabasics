package io.javabrains.unit1;

import java.util.List;

@FunctionalInterface
public interface Lambda {

     void execute(List<Person> people);
}
