package io.javabrains.unit2;

import io.javabrains.unit1.Person;

import java.util.*;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class StandardFunctionalInterfacesExample {

    public static void main(String[] args) {
        List<Person> people = Arrays.asList(
                new Person("Charles","Dickens", 60),
                new Person("Lewis","Carroll", 42),
                new Person("Thomas","Carlyle", 51),
                new Person("Charlotte","Bronte", 60),
                new Person("Matthew","Arnold", 60)
        );
        //Step 1: Sort List by last name
        //The complier knows what type should be there
        Collections.sort(people,( o1, o2)->o1.getSecondName().compareTo(o2.getSecondName()));
        //Step 2: Create a method that prints all elements in the list
        System.out.println("Printing All persons");
        performConditional(people, p -> true, p -> System.out.println(p));
        //Step 3: Create a method that prints all people that have last name beginning with C
        System.out.println("Printing All persons with last name beginning with C");
        performConditional(people, p -> p.getSecondName().startsWith("C"), p -> System.out.println(p));


        performConditional(people, p -> p.getFirstName().startsWith("C"), p -> System.out.println(p.getFirstName()));
    }

    private static void performConditional(List<Person> people, Predicate<Person> predicate, Consumer<Person> consumer) {
        for(Person p : people){
            if(predicate.test(p))
                consumer.accept(p);
        }
    }



}