package io.javabrains.unit2;

import java.util.function.BiConsumer;

public class ExceptionHandling {

    public static void main(String[] args) {
        int [] someNumbers={1,2,3,4};
        int key =2;

        process(someNumbers, key, wrapperLambda((v,k) ->{ System.out.println(v/k);}));


    }
    //wrap types of primitive data types are used for generic types in Java
    private static void process(int[] someNumbers, int key, BiConsumer<Integer,Integer> consumer) {
        for(int i : someNumbers){
            //The behaviour of the function could change , so a general try catch block would not apply, because the exception could be anything
            //The functionality being passed in and executed here is created in the wrapperLambda function
            //The lambda that we are passing to wrapperLambda is different to the Lambda we are passing to the process function.
            //the code in the lambda passed to the wrapper lambda is only executed once the call below is made.
            //a lambda is used to execute another lambda
                consumer.accept(i, key);
        }

    }

    private static BiConsumer<Integer,Integer> wrapperLambda(BiConsumer<Integer,Integer> consumer){
        //This is a true wrapper. we are creating a Lambda and exceuting the lambda that is passed in
        return (v,k)-> {
            try{
                System.out.println("Executing from wrapper");
                consumer.accept(v,k);
            }catch(ArithmeticException e){
                System.out.println("Exception caught in the wrapper Lambda");
            }
        };
    }


}
