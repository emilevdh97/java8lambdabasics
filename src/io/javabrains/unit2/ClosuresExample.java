package io.javabrains.unit2;

public class ClosuresExample {

    public static void main(String[] args) {
        int a = 10;
        //b cannot be changed inside the implementation of process and consider as final by the compiler
        // the compiler is trusting us not to change the variable but will catch us if we do
        // this is similiar to the concept of closures
        int b = 20;
        doProcess(a, i -> System.out.println(i +b));
        //When the compiler parse in the Lambda expression. It freezes the Lambda expression as well as the value of B , concept of closure

    }

    public static void doProcess(int i , Process p){
        p.process(i);
    }
}

interface Process{
    void process(int i );
}