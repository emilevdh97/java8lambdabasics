package io.javabrains.unit2;

public class ThisReferenceExample {

    public  void doProcess(int i , Process p){
        p.process(i);
    }

    public void execute(){
        doProcess(10, i -> {
            System.out.println("Value of i is" + i);
            // it will work but it wont refer to the Process but to the object calling the function
            System.out.println(this);
        });

    }

    public static void main(String[] args) {
        ThisReferenceExample thisReferenceExample = new ThisReferenceExample();
//        thisReferenceExample.doProcess(10, new Process() {
//            @Override
//            public void process(int i) {
//                System.out.println("Value of i is" + i);
//                //'this' refers to the instance of the object and in this case it refers to the anonymous inner class
//                //cannot be used outside of the class because if we tried to call it in main, their is no instance of an object being reference.\
        //refers to the instance of the anonymous inner class
//                System.out.println(this);
//            }
//
//            public String toString(){
//                return "This is the anonymous inner class";
//            }
//        });

     /*   thisReferenceExample.doProcess(10, i -> {
          System.out.println("Value of i is" + i);
        //This cannot be used because Java treats the this reference differently, the this refers is not touch by a lambda
          //System.out.println(this);

        });

*/
        thisReferenceExample.execute();
    }
    public String toString(){
        return "This is the thisReferenceExample class instance";
    }
}
// The anonymous inner class overides the this reference and does not happen in the Lambda
